#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

#define N_THREAD 4
#define CAPACITY 100
typedef struct buffer {
	void *data[CAPACITY];
	int in;
	int out;
	pthread_mutex_t mutex;
	pthread_cond_t empty, full;
} buffer;

typedef struct worker_ctx {
	int count;
	pthread_t thread;
} worker_ctx;

buffer queue;
worker_ctx ctx[N_THREAD];

void buffer_init(buffer *buf) {
	pthread_mutex_init(&buf->mutex, NULL);
	pthread_cond_init(&buf->empty, NULL);
	pthread_cond_init(&buf->full, NULL);
}

void buffer_destroy(buffer *buf) {
	pthread_mutex_destroy(&buf->mutex);
	pthread_cond_destroy(&buf->empty);
	pthread_cond_destroy(&buf->full);
}

void* buffer_get(buffer *buf) {
	void *item;
	pthread_mutex_lock(&buf->mutex);
	while (buf->in == buf->out) {
		pthread_cond_wait(&buf->full, &buf->mutex);
	}
	item = buf->data[buf->out];
	buf->out = (buf->out + 1) % CAPACITY;
	pthread_cond_signal(&buf->empty);
	pthread_mutex_unlock(&buf->mutex);
	return item;
}

void buffer_put(buffer *buf, void *item) {
	pthread_mutex_lock(&buf->mutex);
	while ((buf->in + 1) % CAPACITY == buf->out) {
		pthread_cond_wait(&buf->empty, &buf->mutex);
	}
	buf->data[buf->in] = item;
	buf->in = (buf->in + 1) % CAPACITY;
	pthread_cond_signal(&buf->full);
	pthread_mutex_unlock(&buf->mutex);
}


int count_fp(FILE *fp) {
	char line[10240];
	char *read_result;
	int count = 0;
	if (fp == NULL) return 0;
	while ((read_result = fgets(line, sizeof(line), fp)) != NULL) {
		if (strstr(line, "define")) ++count;
	}
	fclose(fp);
	return count;
}

void* worker(void *arg) {
	FILE *fp;
	worker_ctx *ctx = (worker_ctx*)arg;
	ctx->count = 0;
	while ((fp = (FILE*)buffer_get(&queue)) != NULL) {
		ctx->count += count_fp(fp);
	}
	return NULL;
}

void dir_traverse(const char *dir_name) {
	int error;
	DIR *dir;
	struct dirent entry;
	struct dirent *result;
	struct stat st;
	char fullpath[10240];
	FILE *fp;

	dir = opendir(dir_name);
	if (dir == NULL) return;
	for (;;) {
		error = readdir_r(dir, &entry, &result);
		if (error != 0) {
			perror("readdir");
			break;
		}
		if (result == NULL) break;
		if (!strcmp(result->d_name, ".") || !strcmp(result->d_name, "..")) continue;
		sprintf(fullpath, "%s/%s", dir_name, result->d_name);
		if (lstat(fullpath, &st) < 0) {
			perror("lstat");
			continue;
		}
		dir_traverse(fullpath);
		fp = fopen(fullpath, "r");
		if (fp != NULL) buffer_put(&queue, fp);
	}
}

int main() {
	int i;
	int count = 0;
	buffer_init(&queue);
	for (i = 0; i < N_THREAD; i++) {
		pthread_create(&ctx[i].thread, NULL, worker, &ctx[i]);
	}
	dir_traverse("/usr/include");
	for (i = 0; i < N_THREAD; i++) {
		buffer_put(&queue, 0);
	}
	for (i = 0; i < N_THREAD; i++) {
		pthread_join(ctx[i].thread, NULL);
		count += ctx[i].count;
	}
	buffer_destroy(&queue);
	printf("%d\n", count);
	return 0;
}

