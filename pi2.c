#include <stdio.h>
#include <pthread.h>

#define N 4
#define TOTAL 1000 

typedef struct pi_ctx {
	int start;
	int end;
	double sum;
	pthread_t thread;
} pi_ctx;

pi_ctx ctx[N];

void* calc_thread(void *pContext) {
	pi_ctx *ctx = (pi_ctx*)pContext;
	int i;
	ctx->sum = 0;
	for (i = ctx->start; i != ctx->end; ++i) {
		double t = 1.0 / (2 * i + 1);
		if (i % 2 == 1) {
			ctx->sum -= t;
			// printf("-1/%d ", i * 2 + 1);
		} else {
			ctx->sum += t;
			// printf("+1/%d ", i * 2 + 1);
		}
	}
	return NULL;
}

int main() {
	int i;
	double sum = 0;
	for (i = 0; i < N; ++i) {
		ctx[i].start = TOTAL * i / N;
		ctx[i].end = TOTAL * (i + 1) / N; 
		pthread_create(&ctx[i].thread, NULL, &calc_thread, &ctx[i]);
	}
	for (i = 0; i < N; ++i) {
		pthread_join(ctx[i].thread, NULL);
		sum += ctx[i].sum;
	}
	printf("%lf\n", sum);
	return 0;
}

