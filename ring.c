#include <stdio.h>
#include <pthread.h>

#define N 5

typedef struct thread_ctx {
	pthread_t tid;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	int value;
	int id;
} thread_ctx;

thread_ctx ctxes[N];

void* worker(void *arg) {
	thread_ctx *ctx = (thread_ctx*)arg;
	pthread_mutex_lock(&ctx->mutex);
	if (ctx->id == 0) {
		ctx->value = 0;
	} else {
		pthread_cond_wait(&ctx->cond, &ctx->mutex);
	}
	thread_ctx *next_ctx = &ctxes[(ctx->id + 1) % N];
	printf("Thread %d got %d\n", ctx->id, ctx->value);
	pthread_mutex_lock(&next_ctx->mutex);
	next_ctx->value = ctx->value + 1;
	pthread_cond_signal(&next_ctx->cond);
	pthread_mutex_unlock(&next_ctx->mutex);

	if (ctx->id == 0) {
		pthread_cond_wait(&ctx->cond, &ctx->mutex);
		printf("Thread %d got %d\n", ctx->id, ctx->value);
	}
	pthread_mutex_unlock(&ctx->mutex);
	return NULL;
}

int main() {
	int i;
	for (i = 0; i < N; i++) {
		pthread_mutex_init(&ctxes[i].mutex, NULL);
		pthread_cond_init(&ctxes[i].cond, NULL);
		ctxes[i].id = i;
	}
	for (i = 0; i < N; i++) {
		pthread_create(&ctxes[i].tid, NULL, worker, &ctxes[i]);
	}
	for (i = 0; i < N; i++) {
		pthread_join(ctxes[i].tid, NULL);
	}
	return 0;
}

