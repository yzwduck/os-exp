#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

int count_file(const char *fn) {
  char line[10240];
  char *read_result;
  FILE *fp;
  int count = 0;
  fp = fopen(fn, "r");
  if (fp == NULL) return 0;
  while (1) {
    read_result = fgets(line, sizeof(line), fp);
    if (read_result == NULL) break;
    if (ferror(fp)) break;
    if (strstr(line, "define")) count++;
  }
  fclose(fp);
  return count;
}

int dir_traverse(const char *dir_name) {
  int error;
  DIR *dir;
  struct dirent entry;
  struct dirent *result;
  struct stat st;
  int count = 0;
  char fullpath[10240];
  
  dir = opendir(dir_name);
  if (dir == NULL) return 0;
  for (;;) {
    error = readdir_r(dir, &entry, &result);
    if (error != 0) {
      perror("readdir");
      break;
    }
    if (result == NULL) break;
    if (!strcmp(result->d_name, ".") || !strcmp(result->d_name, "..")) continue;
    sprintf(fullpath, "%s/%s", dir_name, result->d_name);
    if (lstat(fullpath, &st) < 0) {
      perror("lstat");
      continue;
    }
    if (S_ISDIR(st.st_mode));
	count += dir_traverse(fullpath);
        count += count_file(fullpath);
  }
  closedir(dir);
  return count;
}

int main() {
  int result = dir_traverse("/usr/include");
  printf("%d\n", result);
  return 0;
}

