#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define RL_BUFSIZE 256
#define PRINT_ERR_ALLOC() 					\
	do { 							\
		fprintf(stderr, "sh: allocation error\n");	\
		exit(EXIT_FAILURE);				\
	} while(0)

typedef struct cvector {
	size_t size;
	size_t length;
	size_t elem_size;
	void *data;
} cvector;

void cvec_init(cvector *vec, size_t elem_size, size_t init_size) {
	vec->data = malloc(elem_size * init_size);
	if (vec->data == NULL) {
		PRINT_ERR_ALLOC();
	} else {
		vec->size = init_size;
	}
	vec->length = 0;
	vec->elem_size = elem_size;
}

void cvec_push(cvector *vec, void *elem) {
	if (vec->length == vec->size) {
		vec->size = vec->size * 2;
		vec->data = realloc(vec->data, vec->size * vec->elem_size);
	}
	if (vec->data == NULL) {
		PRINT_ERR_ALLOC();
	}
	char *ptr = (char*)vec->data + vec->elem_size * vec->length;
	memcpy(ptr, elem, vec->elem_size);
	++vec->length;
}

void cvec_free(cvector *vec) {
	free(vec->data);
	memset(vec, 0, sizeof(*vec));
}

char *sh_read_line() {
	int bufsize = RL_BUFSIZE;
	int position = 0;
	char *buffer = (char*)malloc(sizeof(char) * bufsize);
	int c;

	if (!buffer) {
		PRINT_ERR_ALLOC();
	}
	while (1) {
		c = getchar();
		if (c == EOF || c == '\n') {
			buffer[position] = '\0';
			return buffer;
		} else {
			buffer[position] = c;
		}
		position++;
		
		if (position >= bufsize) {
			bufsize += RL_BUFSIZE;
			buffer = (char*)realloc(buffer, bufsize);
			if (!buffer) {
				PRINT_ERR_ALLOC();
			}
		}
	}
}

int is_space(char ch) {
	return ch == ' ' || ch == '\t';
}

char* str_next_param(char *cmd) {
	char quote = '\0';
	char *ptr_write = cmd;
	while (is_space(*cmd)) ++cmd;
	if (*cmd == '"' || *cmd == '\'') {
		quote = *cmd;
		++cmd;
	}
	while (*cmd && (quote ? *cmd != quote : !is_space(*cmd))) {
		if (*cmd == '\\') {
			++cmd;
			if (*cmd == '\0') break;
			if (quote != '\0') {
				switch (*cmd) {
					case 'n': *cmd = '\n'; break;
					case 't': *cmd = '\t'; break;
				}
			}
		}
		*ptr_write++ = *cmd++;
	}
	if (*cmd != '\0') ++cmd;
	while (is_space(*cmd)) ++cmd;
	*ptr_write = '\0';
	return cmd;
}

void redirect_pipe_stdin(int fd_last_out, int fd_in) {
	if (fd_in != -1) 
		close(fd_last_out);
	else
		fd_in = fd_last_out;
	if (fd_in != 0) {
		close(0);
		dup2(fd_in, 0);
		close(fd_in);
	}
}

void redirect_pipe_stdout(int fd_pipe_out, int fd_out) {
	if (fd_out != -1) 
		close(fd_pipe_out);
	else
		fd_out = fd_pipe_out;
	if (fd_out != 1) {
		close(1);
		dup2(fd_out, 1);
		close(fd_out);
	}
}

void execute_pwd(const char *fmt) {
	char *buf = getcwd(NULL, 0);
	if (buf == NULL) {
		perror("getcwd");
	} else {
		printf(fmt, buf);
		free(buf);
	}
}

int execute_builtin(cvector *vparam) {
	char **argv;
	if (vparam->length == 1) return 0;
	argv = (char**)vparam->data;
	if (strcmp(argv[0], "exit") == 0) {
		exit(0);
		return 1;
	} else if (strcmp(argv[0], "pwd") == 0) {
		execute_pwd("%s\n");
		return 1;
	} else if (strcmp(argv[0], "cd") == 0) {
		chdir(argv[1]);
		return 1;
	}
	return 0;
}

void execute_cmd(char *cmd) {
	pid_t pid;
	char *param;

	// pipe stdio, backup
	int fd_bak_in = dup(0);
	int fd_bak_out = dup(1);
	int fd_last_out = dup(0);

	while (is_space(*cmd)) ++cmd;
	while (*cmd) {
		cvector vparam;
		int fd_in = -1;
		int fd_out = -1;
		cvec_init(&vparam, sizeof(char*), 4);
		while (1) {
			if (*cmd == '\0') break;
			if (*cmd == '<') {
				++cmd;
				param = cmd;
				cmd = str_next_param(cmd);
				if (fd_in != -1) close(fd_in);
				fd_in = open(param, O_RDONLY);
			} else if (*cmd == '>') {
				++cmd;
				param = cmd;
				cmd = str_next_param(cmd);
				if (fd_out != -1) close(fd_out);
				fd_out = open(param, O_CREAT | O_WRONLY, 0664);
			} else {
				param = cmd;
				cmd = str_next_param(cmd);
				cvec_push(&vparam, &param);
			}
		}
		param = NULL;
		cvec_push(&vparam, &param);
		if (execute_builtin(&vparam)) {
		} else {
			pid = fork();
			if (pid == -1) {
				perror("fork");
			} else if (pid == 0) {
				redirect_pipe_stdin(fd_last_out, fd_in);
				redirect_pipe_stdout(1, fd_out);
				if (execute_builtin(&vparam)) {
					// built in command
				} else {
					char **argv = (char**)vparam.data;
					execvp(argv[0], argv);
				}
				exit(0);
			} else {
				close(fd_last_out);
			}
		}
		cvec_free(&vparam);
	}
	int status;
	waitpid(pid, &status, 0);

	// Restore
	close(0);
	close(1);
	dup2(fd_bak_in, 0);
	dup2(fd_bak_out, 1);
	close(fd_bak_in);
	close(fd_bak_out);
}

int main() {
	fprintf(stderr, "This is exp-shell\n\n");
	while (1) {
		char *cmd;
		execute_pwd("%s $ ");
		cmd = sh_read_line();
		execute_cmd(cmd);
		free(cmd);
	}
	return 0;
}

