#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define CAPACITY 4

typedef struct {
    int value;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} sema_t;

typedef struct {
    int data[CAPACITY];
    int in;
    int out;
} buffer_t;

void sema_init(sema_t* sema, int value)
{
    sema->value = value;
    pthread_mutex_init(&sema->mutex, NULL);
    pthread_cond_init(&sema->cond, NULL);
}

void sema_wait(sema_t* sema)
{
    pthread_mutex_lock(&sema->mutex);
    sema->value--;
    while (sema->value < 0)
        pthread_cond_wait(&sema->cond, &sema->mutex);
    pthread_mutex_unlock(&sema->mutex);
}

void sema_signal(sema_t* sema)
{
    pthread_mutex_lock(&sema->mutex);
    ++sema->value;
    pthread_cond_signal(&sema->cond);
    pthread_mutex_unlock(&sema->mutex);
}

int buffer_is_empty(buffer_t* buf)
{
    return buf->in == buf->out;
}

int buffer_is_full(buffer_t* buf)
{
    return (buf->in + 1) % CAPACITY == buf->out;
}

int get_item(buffer_t* buf)
{
    int item;

    item = buf->data[buf->out];
    buf->out = (buf->out + 1) % CAPACITY;
    return item;
}

void put_item(buffer_t* buf, int item)
{
    buf->data[buf->in] = item;
    buf->in = (buf->in + 1) % CAPACITY;
}

buffer_t buffer;
sema_t mutex_sema;
sema_t empty_buffer_sema;
sema_t full_buffer_sema;
buffer_t buffer2;
sema_t mutex_sema2;
sema_t empty_buffer_sema2;
sema_t full_buffer_sema2;

#define ITEM_COUNT (CAPACITY * 2)
void* consume(void* arg)
{
    int i;
    int item;

    for (i = 0; i < ITEM_COUNT; i++) {
        sema_wait(&full_buffer_sema2);
        sema_wait(&mutex_sema2);
        item = get_item(&buffer2);

        sema_signal(&mutex_sema2);
        sema_signal(&empty_buffer_sema2);

        printf("    consume item: %c\n", item);
    }
    return NULL;
}

void* calculate(void* arg)
{
    int i;
    int item;

    for (i = 0; i < ITEM_COUNT; i++) {
        sema_wait(&full_buffer_sema);
        sema_wait(&mutex_sema);
        item = get_item(&buffer);

        sema_signal(&mutex_sema);
        sema_signal(&empty_buffer_sema);

        sema_wait(&empty_buffer_sema2);
        sema_wait(&mutex_sema2);

        item = item + 'A' - 'a';
        put_item(&buffer2, item);

        sema_signal(&mutex_sema2);
        sema_signal(&full_buffer_sema2);

        printf("    calculate item: %c\n", item);
    }
    return NULL;
}

void produce()
{
    int i;
    int item;

    for (i = 0; i < ITEM_COUNT; i++) {
        sema_wait(&empty_buffer_sema);
        sema_wait(&mutex_sema);

        item = i + 'a';
        put_item(&buffer, item);

        sema_signal(&mutex_sema);
        sema_signal(&full_buffer_sema);

        printf("produce item: %c\n", item);
    }
}

int main()
{
    pthread_t consumer_tid, calculator_tid;

    sema_init(&mutex_sema, 1);
    sema_init(&empty_buffer_sema, CAPACITY - 1);
    sema_init(&full_buffer_sema, 0);
    sema_init(&mutex_sema2, 1);
    sema_init(&empty_buffer_sema2, CAPACITY - 1);
    sema_init(&full_buffer_sema2, 0);

    pthread_create(&consumer_tid, NULL, consume, NULL);
    pthread_create(&calculator_tid, NULL, calculate, NULL);
    produce();
    pthread_join(consumer_tid, NULL);
    pthread_join(calculator_tid, NULL);
    return 0;
}
