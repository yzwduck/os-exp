#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>

#define N 2

typedef struct sort_ctx {
	int start;
	int end;
	pthread_t thread;
} sort_ctx;

#define count_d 6
int d[] = {5,4,1,12,51,5};
int dm[count_d];
sort_ctx ctx[N];

void *sel_sort(void *pCtx) {
	sort_ctx * ctx = (sort_ctx*)pCtx;
	int i, j, m, t;
	for (i = ctx->start; i != ctx->end; ++i) {
		m = i;
		for (j = i + 1; j != ctx->end; ++j) {
			if (d[j] < d[m]) m = j;
		}
		t = d[m];
		d[m] = d[i];
		d[i] = t;
	}
	return NULL;
}

void merge() {
	int i, j, min_v, min_i;
	for (i = 0; i < count_d; ++i) {
		min_v = 1 << 31 - 1;
		for (j = 0; j < N; ++j) {
			if (ctx[j].start < ctx[j].end) {
				int t = ctx[j].start;
				if (d[t] < min_v) {
					min_v = d[t];
					min_i = j;
				}
			}
		}
		dm[i] = min_v;
		ctx[min_i].start++;
	}
}

int main() {
	int i;
	ctx[0].start = 0;
	ctx[0].end = count_d / 2;
	ctx[1].start = count_d / 2;
	ctx[1].end = count_d; 
	pthread_create(&ctx[1].thread, NULL, sel_sort, &ctx[1]);
	sel_sort(&ctx[0]);
	pthread_join(ctx[1].thread, NULL);
	merge();
	for (i = 0; i < count_d; ++i) printf("%d\n", dm[i]);
	return 0;
}

