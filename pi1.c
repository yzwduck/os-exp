#include <stdio.h>
#include <pthread.h>

#define TOTAL 1000 

typedef struct pi_ctx {
	int start;
	int end;
	double sum;
	pthread_t thread;
} pi_ctx;

pi_ctx ctx[2];

void* calc_thread(void *pContext) {
	pi_ctx *ctx = (pi_ctx*)pContext;
	int i;
	ctx->sum = 0;
	for (i = ctx->start; i != ctx->end; ++i) {
		double t = 1.0 / (2 * i + 1);
		if (i % 2 == 1) {
			ctx->sum -= t;
			// printf("-1/%d ", i * 2 + 1);
		} else {
			ctx->sum += t;
			// printf("+1/%d ", i * 2 + 1);
		}
	}
	return NULL;
}

int main() {
	int i;
	double sum = 0;
	ctx[0].start = 0;
	ctx[0].end = TOTAL / 2;
	ctx[1].start = TOTAL / 2;
	ctx[1].end = TOTAL;
	pthread_create(&ctx[1].thread, NULL, &calc_thread, &ctx[1]);
	calc_thread(&ctx[0]);
	pthread_join(ctx[1].thread, NULL);
	sum = ctx[0].sum + ctx[1].sum;
	printf("%lf\n", sum);
	return 0;
}

