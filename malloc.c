#include <stdio.h>

#define POINTERS 1000
#define REGION_SIZE (4000 + 1000*(sizeof(int*) * 2 + sizeof(int) * 2))

char region[REGION_SIZE];

struct block {
    struct block *next;
    struct block *prev;
    int in_use;
    int size;
};

typedef struct block block;

struct block free_list;

void my_malloc_init()
{
    struct block *bloc = (block*)region;

    bloc->in_use = 0;
    bloc->size = REGION_SIZE;
    // add block to free_list
    bloc->next = NULL;
    bloc->prev = NULL;
}

void *my_malloc(int size)
{
	block *cnode, *nnode;
	for (cnode = (block*)region; cnode != NULL; cnode = cnode->next) {
		if (!cnode->in_use && cnode->size >= size + sizeof(block)) {
			break;
		}
	}
	if (cnode == NULL) return NULL;
	if (cnode->size > size + 2 * sizeof(block)) {
		nnode = (block*)((char*)cnode + sizeof(block) + size);
		nnode->size = cnode->size - size - sizeof(block);
		nnode->in_use = 0;
		nnode->prev = cnode;
		nnode->next = cnode->next;
		cnode->next = nnode;
	}
	cnode->in_use = 1;
	cnode->size = sizeof(block) + size;
	return (char*)cnode + sizeof(block);
}

void my_free(void *p)
{
	char *ptr = (char*)p - sizeof(block);
	block *cnode, *nnode;
	for (cnode = (block*)region; cnode != NULL; cnode = cnode->next) {
		if ((char*)cnode > ptr) return;
		if ((char*)cnode == ptr) break;
	}
	if (!cnode->in_use) return;
	cnode->in_use = 0;
	if (cnode->next && !cnode->next->in_use) {
		nnode = cnode->next;
		cnode->next = nnode->next;
		cnode->size += nnode->size;
		if (cnode->next) cnode->next->prev = cnode;
	}
	if (cnode->prev && !cnode->prev->in_use) {
		nnode = cnode->prev;
		nnode->next = cnode->next;
		nnode->size += cnode->size;
		if (nnode->next) nnode->next->prev = nnode;
	}
}

void print_free_memory()
{
	block *cnode;
	for (cnode = (block*)region; cnode != NULL; cnode = cnode->next) {
		char *end = (char*)cnode + cnode->size;
		char *status = cnode->in_use?"IN_USE":"FREE";
		printf("[%p-%p] %s\n", cnode, end, status);
	}
	puts("");
}

void test0()
{
    int size;
    void *p1, *p2;

    puts("Test0");

    p1 = my_malloc(10);
    print_free_memory();

    p2 = my_malloc(20);
    print_free_memory();

    my_free(p1);
    print_free_memory();

    my_free(p2);
    print_free_memory();
}

void test1()
{
    void *array[POINTERS];
    int i;
    void *p;

    puts("Test1");
    for (i = 0; i < POINTERS; i++) {
        p = my_malloc(4);
        array[i] = p;
    }

    for (i = 0; i < POINTERS; i++) {
        p = array[i];
        my_free(p);
    }

    print_free_memory();
}

void test2()
{
    void *array[POINTERS];
    int i;
    void *p;

    puts("Test1");
    for (i = 0; i < POINTERS; i++) {
        p = my_malloc(4);
        array[i] = p;
    }

    for (i = POINTERS - 1; i >= 0; i--) {
        p = array[i];
        my_free(p);
    }

    print_free_memory();
}

void test3()
{
    void *array[POINTERS];
    int i;
    void *p;

    puts("Test1");
    for (i = 0; i < POINTERS; i++) {
        p = my_malloc(4);
        array[i] = p;
    }

    for (i = 0; i < POINTERS; i += 2) {
        p = array[i];
        my_free(p);
    }

    for (i = 1; i < POINTERS; i += 2) {
        p = array[i];
        my_free(p);
    }

    print_free_memory();
}

int main()
{
    my_malloc_init();
    test0();
    test1();
    test2();
    test3();
    puts("Finished");
    return 0;
}

