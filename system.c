#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int my_system(const char *cmd) {
	int stat;
	pid_t pid;
	if ((pid = fork()) == 0) {
		execl("/bin/sh", "sh", "-c", cmd, (char*)0);
		_exit(127);
	}
	if (pid == -1) {
		stat = -1;
	} else {
		while (waitpid(pid, &stat, 0) == -1) {
			stat = -1;
			break;
		}
	}
	return stat;
}

int main(int argc, char *argv[]) {
	int ret;
	if (argc != 2) {
		printf("Usage: %s 'sys command'\n\n", argv[0]);
		return 1;
	}
	printf("Executing: %s\n", argv[1]);
	ret = my_system(argv[1]);
	printf("\nmy_system return %d\n", ret);
	return ret;
}
 
